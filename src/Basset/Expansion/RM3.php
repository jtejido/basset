<?php


namespace Basset\Expansion;

use Basset\Models\Contracts\WeightedModelInterface;
use Basset\Feature\FeatureInterface;
use Basset\Feature\FeatureVector;

/**
 * An object that wraps the Tokenized Document. It accepts $class as optional mainly for labeling purposes, 
 * otherwise it's null.
 * 
 * @see TokensDocument
 *
 * @var $d
 * @var $class
 *
 * @example new Document(new TokensDocument(array('how', 'do', 'you', 'do?')));
 *
 * @author Jericko Tejido <jtbibliomania@gmail.com>
 */

class RM3 extends Feedback implements PRFInterface
{

    CONST LAMBDA = 0.5;

    private $lambda;

    /**
     * @param int $feedbackdocs
     * @param int $feedbackterms
     */

    public function __construct(int $feedbackdocs = parent::TOP_REL_DOCS, int $feedbackterms = parent::TOP_REL_TERMS, float $lambda = self::LAMBDA)
    {
        parent::__construct($feedbackdocs, $feedbackterms);
        $this->lambda = $lambda;
    }


    /**
     * Expands original query based on array of relevant docs received.
     *
     * @param  array $docIds
     * @return array
     */
    protected function queryExpand(array $docIds): FeatureInterface
    {

        $relevantVector = new FeatureVector;

        $queryVector = $this->getQuery()->getFeature();

        $newDoc = array();

        $totalWeight = array();

        foreach($docIds as $class => $score) {
            $doc = $this->indexsearch->getDocumentVector($class);
            $docVector = $this->transformVector($this->getModel(), $doc)->getFeature();

            foreach($docVector as $key => &$item) {
                $item *= $score;
                if(isset($queryVector[$key])) {
                    $queryVector[$key] = $item;
                }
            }

            $totaldocweight = array_sum($docVector);

            foreach($docVector as $key => $item) {
                $val = ($item / $totaldocweight);
                if(isset($totalWeight[$key])) {
                    $totalWeight[$key] += $val;
                } else {
                    $totalWeight[$key] = $val;
                }

                if(isset($queryVector[$key])) {
                    $totalWeight[$key] = (1-$this->lambda) * $item + ($this->lambda * $val);
                }
            }

        }


        $relevantVector->addTerms($totalWeight);

        return $relevantVector;

    }


    
}
